import styled from "styled-components";

export const Container = styled.div`
  max-width: 1296px;
  padding: 0 20px;
  margin: 0 auto;
`;

import { PrimaryButton } from "@/components/buttons/PrimaryButton";
import React, { FC, ReactNode } from "react";
import styled from "styled-components";
import { Container } from "./common/Container";

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  min-height: 100vh;
`;

const Main = styled.main`
  flex-grow: 1;
`;

const StyledHeadContainer = styled(Container)`
  display: flex;
  justify-content: space-between;
`;

const Header = styled.header`
  padding: 20px 0;
`;

const Nav = styled.nav`
  display: flex;
  gap: 30px;
`;

const NavList = styled.ul`
  display: flex;
  gap: 20px;
  align-items: center;
`;

const Footer = styled.footer`
  padding: 20px 0;
`;

export const Layout: FC<{ children: ReactNode }> = ({ children }) => (
  <Wrapper>
    <Header>
      <StyledHeadContainer>
        <div>LOGO</div>
        <Nav>
          <NavList>
            <li>ХАРАКТЕРИСТИКИ</li>
            <li>ВІДЕООГЛЯД</li>
            <li>ДЛЯ ЧОГО</li>
          </NavList>
          <PrimaryButton>КУПИТИ</PrimaryButton>
        </Nav>
      </StyledHeadContainer>
    </Header>
    <Main>
      <Container>{children}</Container>
    </Main>
    <Footer>
      <Container>контакти, адрес і тд</Container>
    </Footer>
  </Wrapper>
);

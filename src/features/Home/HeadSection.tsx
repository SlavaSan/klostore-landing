import React, { FC } from "react";
import image from "@/assets/trusi.jpg";
import styled from "styled-components";
import { rgba } from "emotion-rgba";
import { PrimaryButton } from "@/components/buttons/PrimaryButton";
import { SecondaryButton } from "@/components/buttons/SecondaryButton";

const Wrapper = styled.div`
  display: flex;
  padding-top: 50px;
  gap: 20px;
  width: 100%;
`;

const DescriptionWrapper = styled.div`
  min-width: 50%;
  display: flex;
  flex-direction: column;
`;

const ImageWrapper = styled.div`
  max-width: 100%;
  margin: 0 30px;
  position: relative;
  border-radius: 40px;
  overflow: hidden;
  height: 700px;
  ::before {
    content: "";
    position: absolute;
    width: 100%;
    height: 100%;
    display: block;
    z-index: 1;
    background: linear-gradient(
      90deg,
      ${({ theme }) => rgba(theme.palette.darkAccent, 0.4)},
      ${({ theme }) => rgba(theme.palette.primary, 0.2)} 70%
    );
  }
`;

const Description = styled.p`
  font-size: 18px;
  margin-bottom: 50px;
`;

const Image = styled.img`
  height: 100%;
`;

const Title = styled.h2`
  margin-top: 50px;
  font-size: 42px;
  margin-bottom: 20px;
  font-weight: 400;
`;

const Discount = styled.p`
  padding: 10px 20px;
  border-radius: 10px;
  background-color: ${({ theme }) => theme.palette.danger};
  width: fit-content;
  margin: 20px 0;
`;

const PriceWrapper = styled.div`
  margin: 0 80px 0 60px;
  display: flex;
  text-align: center;
  height: 100px;
`;

const OldPriceWrapper = styled.div`
  width: 50%;
  height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  position: relative;
  background-color: ${({ theme }) => theme.palette.dark};
  ::before {
    display: block;
    content: "";
    border-left: 44px solid rgba(255, 255, 255, 0);
    border-top: 100px solid ${({ theme }) => theme.palette.dark};
    position: absolute;
    top: 0;
    left: -44px;
  }
  ::after {
    display: block;
    content: "";
    border-left: 44px solid ${({ theme }) => theme.palette.dark};
    border-top: 100px solid ${({ theme }) => theme.palette.primary};
    position: absolute;
    top: 0;
    right: -22px;
    z-index: 10;
  }
`;

const PriceTitle = styled.p`
  font-size: 20px;
  margin-bottom: 8px;
`;
const Price = styled.p<{ lineThrough?: boolean }>`
  font-size: 24px;
  ${({ lineThrough }) => lineThrough && "text-decoration: line-through;"}
`;

const NewPriceWrapper = styled.div`
  width: 50%;
  height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  position: relative;
  ::before {
    display: block;
    content: "";
    border-right: 44px solid rgba(255, 255, 255, 0);
    border-bottom: 100px solid ${({ theme }) => theme.palette.primary};
    position: absolute;
    top: 0;
    right: -44px;
  }
  background-color: ${({ theme }) => theme.palette.primary};
`;

const ActionsWrapper = styled.div`
  margin-top: auto;
  display: grid;
  gap: 40px;
  grid-template-columns: repeat(2, 1fr);
  margin-bottom: 40px;
`;

export const HeadSection: FC = () => (
  <Wrapper>
    <DescriptionWrapper>
      <Title>Труси для сексу</Title>
      <Discount>Знижка: 40%</Discount>
      <Description>
        Вікин сікрєт, не видно піську і сіську не взнаєш поки не знімеш
      </Description>
      <PriceWrapper>
        <OldPriceWrapper>
          <PriceTitle>Звичийна ціна</PriceTitle>
          <Price lineThrough>1300грн</Price>
        </OldPriceWrapper>
        <NewPriceWrapper>
          <PriceTitle>Сьогодні</PriceTitle>
          <Price>790грн</Price>
        </NewPriceWrapper>
      </PriceWrapper>
      <ActionsWrapper>
        <PrimaryButton>Замовити</PrimaryButton>
        <SecondaryButton>Детальніше</SecondaryButton>
      </ActionsWrapper>
    </DescriptionWrapper>
    <ImageWrapper>
      <Image src={image.src} alt="труси" />
    </ImageWrapper>
  </Wrapper>
);

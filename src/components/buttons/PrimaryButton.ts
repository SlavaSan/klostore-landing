import styled from "styled-components";

export const PrimaryButton = styled.button`
  background-color: ${({ theme }) => theme.palette.dark};
  color: ${({ theme }) => theme.palette.whiteAccent};
  border: 1px solid ${({ theme }) => theme.palette.whiteAccent};
  border-radius: 10px;
  height: 48px;
  padding: 10px 20px;
  cursor: pointer;
  transition: 300ms;
  :hover {
    transform: scale(1.2, 1.1);
  }
`;

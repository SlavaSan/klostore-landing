import styled from "styled-components";

export const SecondaryButton = styled.button`
  background-color: ${({ theme }) => theme.palette.light};
  color: ${({ theme }) => theme.palette.darkAccent};
  border: 2px solid ${({ theme }) => theme.palette.darkAccent};
  border-radius: 10px;
  height: 48px;
  padding: 10px 20px;
  cursor: pointer;
  transition: 300ms;
  :hover {
    transform: scale(1.2, 1.1);
  }
`;

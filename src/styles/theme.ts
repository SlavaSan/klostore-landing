import { DefaultTheme } from "styled-components";

export const defaultColorConfig = {};

export type TTheme = "light";

type TThemingConfig = Record<TTheme, DefaultTheme>;

export const theming: TThemingConfig = {
  light: {
    palette: {
      currentTheme: "light",
      primary: "#ba68c8",
      light: "#ee98fb",
      dark: "#883997",
      whiteAccent: "#fff",
      darkAccent: "#000",
      danger: "#f44336",
    },
  },
};

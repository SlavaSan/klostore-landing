import "styled-components";

// Inspired by https://medium.com/rbi-tech/theme-with-styled-components-and-typescript-209244ec15a3

interface IPalette {
  currentTheme: "light";
  primary: string;
  light: string;
  dark: string;
  whiteAccent: string;
  darkAccent: string;
  danger: string;
}

declare module "styled-components" {
  export interface DefaultTheme {
    palette: IPalette;
  }
}

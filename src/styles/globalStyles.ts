// globalStyles.js
import { createGlobalStyle } from "styled-components";

export const GlobalStyle = createGlobalStyle`
  * {
    box-sizing: border-box;
  }
  body {
    background: linear-gradient(90deg, ${({ theme }) =>
      theme.palette.primary}, ${({ theme }) => theme.palette.light} 50%, ${({
  theme,
}) => theme.palette.primary});
    font-family: 'Philosopher', sans-serif;
    color: ${({ theme }) => theme.palette.whiteAccent};
    margin: 0;
    min-height: 100vh;
  }
  h1, h2, h3, h4, h5, h6, p {
    margin: 0;
  }
  img {
    display: block;
  }
  ul {
    padding: 0;
    margin: 0;
    list-style: none;
  }
  svg {
    transition: 300ms;
  }
`;

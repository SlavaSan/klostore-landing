import type { AppProps } from "next/app";
import { ThemeAppProvider } from "@/styles/ThemeProvider";
import { GlobalStyle } from "@/styles/globalStyles";

interface CustomAppProps extends Omit<AppProps, "Component"> {
  Component: AppProps["Component"] & { Layout: JSX.Element };
}

function MyApp({ Component, pageProps }: CustomAppProps) {
  return (
    <ThemeAppProvider theme="light">
      <GlobalStyle />
      <Component {...pageProps} />
    </ThemeAppProvider>
  );
}

export default MyApp;

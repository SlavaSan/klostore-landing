import { HeadSection } from "@/features/Home/HeadSection";
import { Layout } from "@/layouts/Layout";
import type { NextPage } from "next";

const Home: NextPage = () => (
  <Layout>
    <HeadSection />
  </Layout>
);

export default Home;
